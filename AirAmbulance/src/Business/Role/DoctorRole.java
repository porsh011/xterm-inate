/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Network.Network;
import Business.Organization.DoctorOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.DoctorRole.DoctorWorkAreaJPanel;


/**
 *
 * @author adith
 */
public class DoctorRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization,Enterprise enterprise,EnterpriseDirectory dir, EcoSystem business,Network network) {
        //return new HospitalAdminWorkAreaJPanel(userProcessContainer, enterprise);
        return new DoctorWorkAreaJPanel(userProcessContainer, account, (DoctorOrganization) organization, dir,enterprise);
    }

     @Override
    public String toString(){
        return RoleType.DoctorRole.getValue();
    }

    
    
}
