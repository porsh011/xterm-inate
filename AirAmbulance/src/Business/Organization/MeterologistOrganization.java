/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.MeterologistRole;
import Business.Role.Role;
import java.util.HashSet;

/**
 *
 * @author Pornima
 */
public class MeterologistOrganization extends Organization {
    public MeterologistOrganization() {
       
        super(Organization.Type.Meterologist.getValue());
         
    }

    
    
    @Override
    public HashSet<Role> getSupportedRole() {
        roles= new HashSet<>();
        roles.add(new MeterologistRole() );
       
        return roles;
    }
    
}
