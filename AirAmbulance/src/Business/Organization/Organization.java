/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

//<<<<<<< HEAD
import Business.ContingencyAdmin.ContingencyDirectory;
//=======
import Business.AirAmbulance.AirAmbulanceDirectory;
//>>>>>>> d8d76b7167182f52d03019b8e7f15e48fdcd3072
import Business.Employee.EmployeeDirectory;
import Business.Hospital.HospitalDirectory;
import Business.MedicalTeam.MedicalTeamDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.HashSet;
//<<<<<<< HEAD
import Business.Cryogenics.CryogenicDirectory;
import Business.SupplyAdmin.SupplyAdminDirectory;
//=======
//>>>>>>> d8d76b7167182f52d03019b8e7f15e48fdcd3072

/**
 *
 * @author adith
 */
public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private HospitalDirectory hospitalDirectory;
//<<<<<<< HEAD
    private CryogenicDirectory cryogenicDirectory;
    private SupplyAdminDirectory supplyadmindir;
    private ContingencyDirectory contingencyDirectory;
    
//=======
    private AirAmbulanceDirectory airambulancedir;
    private MedicalTeamDirectory medDirectory;
//>>>>>>> d8d76b7167182f52d03019b8e7f15e48fdcd3072
    private int organizationID;
    private static int counter=0;
    public HashSet<Role> roles;

    
    
    public enum Type{
        Lab("Lab Organization"),Pharmacy("Pharmacy Organization"),Doctor("Doctor Organization"),AirTrafficController("AirTrafficController Organization"), Admin("Admin Organization"), Cryogenics("Cryogenics Organization"), Pilot("Pilot Organization"), MedicalTeam("MedicalTeam Organization"), Meterologist("Meterologist Organization"), Safety("Safety Organization"), Mayor("Mayor Organization"), ContingencyAdmin("Contingency Organization"), SupplyAdmin("FoodSupply Organization" );
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        roles = new HashSet<>();
        hospitalDirectory=new HospitalDirectory();
//<<<<<<< HEAD
        cryogenicDirectory=new CryogenicDirectory();
        supplyadmindir = new SupplyAdminDirectory();
        contingencyDirectory = new ContingencyDirectory();
//=======
        airambulancedir = new AirAmbulanceDirectory();
        medDirectory=new MedicalTeamDirectory();
//>>>>>>> d8d76b7167182f52d03019b8e7f15e48fdcd3072
        ++counter;
    }

    public abstract HashSet<Role> getSupportedRole();

    public AirAmbulanceDirectory getAirambulancedir() {
        return airambulancedir;
    }

    public void setAirambulancedir(AirAmbulanceDirectory airambulancedir) {
        this.airambulancedir = airambulancedir;
    }

    
    
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public HospitalDirectory getHospitalDirectory() {
        return hospitalDirectory;
    }

    public void setHospitalDirectory(HospitalDirectory hospitalDirectory) {
        this.hospitalDirectory = hospitalDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }
    
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public MedicalTeamDirectory getMedDirectory() {
        return medDirectory;
    }

    public void setMedDirectory(MedicalTeamDirectory medDirectory) {
        this.medDirectory = medDirectory;
    }
    

    @Override
    public String toString() {
        return name;
    }
    
    
}
