/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Pilot.PilotDirectory;
import Business.Role.PilotRole;
import Business.Role.Role;
import java.util.HashSet;

/**
 *
 * @author Pornima
 */
public class PilotOrganization extends Organization {
    private PilotDirectory pilotdir;
    public PilotOrganization() {
        
       
        super(Organization.Type.Pilot.getValue());
         
    }

    public PilotDirectory getPilotdir() {
        return pilotdir;
    }

    public void setPilotdir(PilotDirectory pilotdir) {
        this.pilotdir = pilotdir;
    }

    
    
    @Override
    public HashSet<Role> getSupportedRole() {
        roles= new HashSet<>();
        roles.add(new PilotRole() );
       
        return roles;
    }
    
}
