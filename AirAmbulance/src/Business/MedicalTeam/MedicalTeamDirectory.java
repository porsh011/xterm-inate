/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.MedicalTeam;

import java.util.ArrayList;

/**
 *
 * @author Pornima
 */
public class MedicalTeamDirectory {
     private ArrayList<MedicalTeam> medicalTeamList;

    public MedicalTeamDirectory() {
        medicalTeamList = new ArrayList<>();
    }

    public ArrayList<MedicalTeam> getMedicalTeamList() {
        return medicalTeamList;
    }

    public void setMedicalTeamList(ArrayList<MedicalTeam> medicalTeamList) {
        this.medicalTeamList = medicalTeamList;
    }

    
    
    public MedicalTeam createTeam(String name){
        MedicalTeam team = new MedicalTeam();
        team.setTeamName(name);
        medicalTeamList.add(team);
        return team;
    }

    
}
