/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Meterologist;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

/**
 *
 * @author Pornima
 */
public class Meterologist {
        private String wind_mph;
	private String temp_c;
	private String precip_in;
        private String cloud;
        private String name;
	private String region;
	private String country;

        public Meterologist(){
            populatedData();
        }
                
        public String getWind_mph() {
		return wind_mph;
	}
	public void setWind_mph(String wind_mph) {
		this.wind_mph = wind_mph;
	}

    public String getCloud() {
        return cloud;
    }

    public void setCloud(String cloud) {
        this.cloud = cloud;
    }

    public String getTemp_c() {
        return temp_c;
    }

    public void setTemp_c(String temp_c) {
        this.temp_c = temp_c;
    }

    public String getPrecip_in() {
        return precip_in;
    }

    public void setPrecip_in(String precip_in) {
        this.precip_in = precip_in;
    }
	
        public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

        private static ObjectMapper mapper = new ObjectMapper();
	public void populatedData() {

		try {
			String url = "http://api.apixu.com/v1/current.json?key=96eee5b2085c4cdea00234340170712&q=Boston";
                        InputStream is = new URL(url).openStream();
                                             

			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                       	String jsonText = "{\"location\":{\"name\":\"Boston\",\"region\":\"Massachusetts\",\"country\":\"United States of America\",\"lat\":42.36,\"lon\":-71.06,\"tz_id\":\"America/New_York\",\"localtime_epoch\":1512914500,\"localtime\":\"2017-12-10 9:01\"},\"current\":{\"last_updated_epoch\":1512913509,\"last_updated\":\"2017-12-10 08:45\",\"temp_c\":0.0,\"temp_f\":32.0,\"is_day\":1,\"condition\":{\"text\":\"Partly cloudy\",\"icon\":\"//cdn.apixu.com/weather/64x64/day/116.png\",\"code\":1003},\"wind_mph\":16.1,\"wind_kph\":25.9,\"wind_degree\":270,\"wind_dir\":\"W\",\"pressure_mb\":1007.0,\"pressure_in\":30.2,\"precip_mm\":0.0,\"precip_in\":0.0,\"humidity\":81,\"cloud\":75,\"feelslike_c\":-6.0,\"feelslike_f\":21.2,\"vis_km\":16.0,\"vis_miles\":9.0}}";
                       
                        
                        
			WeatherData weatherData = mapper.readValue(jsonText, WeatherData.class);

                        wind_mph = weatherData.getCurrent().getWind_mph();
                        temp_c=weatherData.getCurrent().getTemp_c();
                        cloud = weatherData.getCurrent().getCloud();
                        name=weatherData.getLocation().getName();
                        country=weatherData.getLocation().getCountry();
                        region=weatherData.getLocation().getRegion();
		} catch (Exception e) {
			//e.printStackTrace();
                        System.out.println("server not available!!please check for internet connection");
                }
        }
	
    @Override
    public String toString()
    {
        return wind_mph;
    }
    
}
