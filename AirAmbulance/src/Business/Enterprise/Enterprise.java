/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Cryogenics.CryogenicDirectory;
import Business.Meterologist.Meterologist;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;

/**
 *
 * @author adith
 */
public abstract class Enterprise extends Organization{

    private EnterpriseType enterpriseType;
    private OrganizationDirectory organizationDirectory;
    private Meterologist meterologist;
    private CryogenicDirectory cryogenicsdir;
    
    public Enterprise(String name, EnterpriseType type) {
        super(name);
        this.enterpriseType = type;
        organizationDirectory = new OrganizationDirectory();
        meterologist=new Meterologist();
    }
    
    public enum EnterpriseType{
        Hospital("Hospital"),Government("Government"),AirAmbulance("AirAmbulance");
        
        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
        

        @Override
        public String toString() {
            return value;
        }
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public void setOrganizationDirectory(OrganizationDirectory organizationDirectory) {
        this.organizationDirectory = organizationDirectory;
    }
    

    public Meterologist getMeterologist() {
        return meterologist;
    }

    public void setMeterologist(Meterologist meterologist) {
        this.meterologist = meterologist;
    }

    public CryogenicDirectory getCryogenicsdir() {
        return cryogenicsdir;
    }

    public void setCryogenicsdir(CryogenicDirectory cryogenicsdir) {
        this.cryogenicsdir = cryogenicsdir;
    }
    

    
}
