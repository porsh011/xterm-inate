/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

//import Business.Meterologist.MeterologistDirectory;
import Business.Cryogenics.CryogenicDirectory;
import Business.Pilot.PilotDirectory;
import Business.Role.AirAmbulanceAdminRole;
import Business.Role.CryogenicsHeadRole;
import Business.Role.MeterologistRole;
import Business.Role.PilotRole;
import Business.Role.Role;
import Business.Role.SafetyRole;
import Business.SafetyAdmin.SupplyItemsDirectory;
import java.util.HashSet;

/**
 *
 * @author adith
 */
public class AirAmbulanceEnterprise extends Enterprise{
    private PilotDirectory pilotdir;
    private CryogenicDirectory cryodir;
    private SupplyItemsDirectory supplydir;
    
    
    AirAmbulanceEnterprise(String name) {
        super(name, Enterprise.EnterpriseType.AirAmbulance);
        pilotdir = new PilotDirectory();
        cryodir=new CryogenicDirectory();
        supplydir=new SupplyItemsDirectory();
        
        
        
    }

    
    

    public PilotDirectory getPilotdir() {
        return pilotdir;
    }

    public void setPilotdir(PilotDirectory pilotdir) {
        this.pilotdir = pilotdir;
    }

    public CryogenicDirectory getCryodir() {
        return cryodir;
    }

    public void setCryodir(CryogenicDirectory cryodir) {
        this.cryodir = cryodir;
    }

    public SupplyItemsDirectory getSupplydir() {
        return supplydir;
    }

    public void setSupplydir(SupplyItemsDirectory supplydir) {
        this.supplydir = supplydir;
    }
    

    

    @Override
    public HashSet<Role> getSupportedRole() {
         roles= new HashSet<>();
        roles.add(new AirAmbulanceAdminRole());
        roles.add(new PilotRole());
        roles.add(new MeterologistRole());
        roles.add(new SafetyRole());
        roles.add(new CryogenicsHeadRole());
        return roles;
    }
}
